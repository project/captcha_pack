# CAPTCHA Pack

The CAPTCHA Pack module contains several CAPTCHA types for use with the CAPTCHA module.
The CAPTCHA Pack module is meant to provide lightweight,
yet effective alternatives for the traditional image CAPTCHA, which is undesirable in certain situations
(e.g. bandwidth restrictions, cpu restrictions, accessibility constraints, etc).

For a full description of the module, visit the
[project page](https://www.drupal.org/project/captcha_pack).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/captcha_pack).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

* [CAPTCHA](https://www.drupal.org/project/captcha)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Configure Captcha types:
Home >> Administration >> Configuration >> People
(/admin/config/people/captcha)


## Maintainers

- Oleksandr Lunov - [alunyov](https://www.drupal.org/u/alunyov)
- Ev Maslovskiy - [spleshka](https://www.drupal.org/u/spleshka)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
